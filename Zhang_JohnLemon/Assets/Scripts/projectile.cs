﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        
        if (!other.isTrigger)
        {
            //when projectile collides with objects with "enemy" tag, it will reduce objects' health.
            if (other.gameObject.CompareTag("Enemy"))
            {
                
                EnemyHealth eHealth = other.gameObject.GetComponent<EnemyHealth>();

                if (eHealth != null)
                    eHealth.TakeDamage(1);         
            }
            Destroy(gameObject);
        }
    }

}