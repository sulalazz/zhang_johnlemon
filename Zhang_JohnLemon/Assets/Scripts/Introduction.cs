﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//this script is for introducing the game to the player and tell them the rule
public class Introduction : MonoBehaviour
{
    public Text introductionText;
    // Start is called before the first frame update
    void Start()
    {
        introductionText.text = "Welcome to John Lemon's Haunted Jaunt! This is a 3D escape game, you need to escape the room to win the game! Be careful about the ghosts here, they will catch you! You can follow the sound to find lemon and press Enter to attck the enemies. Remember, you have to hit enemies twice to kill them. Now, press any key to start your adventure, good luck!";
        Time.timeScale = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        //player can pree anykey to start the game
        if (Input.anyKeyDown)
        {
            Time.timeScale = 1f;
            gameObject.SetActive(false);
        }
    }
}