﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEnding : MonoBehaviour
{
    //timer setup
    float currentTime = 0f;
    float startingTime = 60f;

    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public CanvasGroup timeoutBackgroundImageCanvasGroup;
    public Text timerText;
    public Text restartText;

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;

    void Start()
    {
        restartText.text = "Press Enter to restart";
        currentTime = startingTime;

    }

    void OnTriggerEnter(Collider other)
    {
        //player at the exit point
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
        }
    }

    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }

    void Update()
    {
        if (m_IsPlayerAtExit == false || m_IsPlayerCaught == false)
        {
            //timer works when both John not at the exit point and John has not been caught
            currentTime -= 1 * Time.deltaTime;
            timerText.text = "Time left: " + currentTime.ToString("0");
        }

        //John is at exit point and the exit win pic and audio come out
        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        //John is been caught, the caught image and audio come out
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
        //if the countdown timer is 0, come up the game over image
        if (currentTime <= 0)
        {
            currentTime = 0;
            EndLevel(timeoutBackgroundImageCanvasGroup, true, caughtAudio);
            //loseText.text = "Game Over :(";
        }

        if (Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene(0);
            }
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        //audio works with the code here
        if(!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }

        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            Application.Quit();
            /*if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }*/
        }
    }
}