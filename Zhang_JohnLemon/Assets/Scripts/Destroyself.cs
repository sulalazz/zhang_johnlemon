﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyself : MonoBehaviour
{
    public float wait;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Destroyme", wait);
    }

    private void Destroyme()
    {
        Destroy(gameObject);
    }
}
