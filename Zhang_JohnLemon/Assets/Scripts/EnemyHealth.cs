﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int health = 2;
    public GameObject hitAudioPrefab;

    public void TakeDamage(int damageAmount)
    {
        health -= damageAmount;

        if (health <= 0)
        {
            Destroy(gameObject);   
        }
        Instantiate(hitAudioPrefab, transform.position, hitAudioPrefab.transform.rotation);
    }
}
