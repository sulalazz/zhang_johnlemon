﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{


    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    //counting number for lemon pickup
    int m_Lemons;

    //default setting that John Lemon cannot shoot the projectile without picking up at least one lemon.
    bool haveShooting = false;

    //projectile setting
    public GameObject projectilePrefab;
    public Transform shotSpawn;
    public float shotSpeed = 5f;


    // Start is called before the first frame update
    void Start()
    {
        //at first, the number of lemon is 0
        m_Lemons = 0;

        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();


    }


    private void Update()
    {
       
        //when pressing the key and the lemon number is greater than 0, John Lemon can shoot projectile. The projectile number should control by the pickup number.
        if (haveShooting && m_Lemons >0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameObject projectile = Instantiate(projectilePrefab, shotSpawn.transform.position, projectilePrefab.transform.rotation);
                Rigidbody projectileRB = projectile.GetComponent<Rigidbody>();
                projectileRB.velocity = transform.forward * shotSpeed;
                m_Lemons--;
            }
        }
           
    }

    private void OnTriggerEnter(Collider other)
    {
        //when John Lemon collecting lemon, it should destroy the object in the ground
        if (other.gameObject.CompareTag("Weapon"))
        {
            haveShooting =true;
            Destroy(other.gameObject);
            m_Lemons++;
        }
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        //is walking and is stopping sound
        if(isWalking)
        {
            if(!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation (desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    public static implicit operator PlayerMovement(bool v)
    {
        throw new NotImplementedException();
    }
}
